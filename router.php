<?php
namespace App;
use App\Controller\HomeController;
use App\Controller\MessageController;
use App\Model\DB;
use http\Params;

//use App\Model\Message;


require_once __DIR__ . '/vendor/autoload.php';


/*include './View/baseTemplate.php';*/
//include './Model/DB.php';
//include './Model/Message.php';
//include './Controller/MessageController.php';
//include './Controller/HomeController.php';

session_start();
$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$config = include __DIR__ . '/src/App/config/config.php';
$db = new DB($config['DB_NAME'], $config['DB_USERNAME'], $config['DB_PASSWORD']);
$data = [
    'post' => $_POST,
    'server' => $_SERVER,
    'get' => $_GET,
    'files' => $_FILES
];
$id = 1;

switch ($url) {
    case '/':
        $homeController = new HomeController;
        $homeController->menu();
        break;
    case '/create':
        $messageController = new MessageController($db, $data);
        $messageController->createMessage();
        break;
    case '/create-form':
        $messageController = new MessageController($db, $data);
        $messageController->createForm();
        break;
    case '/show':
        $messageController = new MessageController($db, $data);
        $messageController->show();
        break;
    case "/edit":
        $messageController = new MessageController($db, $data);
        $messageController->edit();
        break;
    case "/update":
        $messageController = new MessageController($db, $data);
        $messageController->update();
        break;
    default:
        $homeController = new HomeController();
        $homeController->default();
        break;
}

