# Проект "Гостевая книга"
## Работу выполнил Толпыго Павел

Для развертывания проекта потребуется локальное окружение - OpenServer  
или Laragon.
Версии технологий которые используются в проекте.
```
   PHP-8.2.0
   Apache-2.4.35
   MySQL-5.7.24
   Composer-2.5.1
```
***ВАЖНО! Убедиться в подключенности расширения pdo_mysql в файле php.ini***  

Перед первоначальным запуском требуется:   
1.  Склонировать репозиторий с gitlab - git@gitlab.com:HEIZ23/guest-book.git и перейти в папку с проектом
2.  Выполнить команду в консоли ***composer install***
2.  Выполнить скрипт messagesTable.sql, который располагается src/App/database/messagesTable.sql

Список доступных маршрутов:
1. ***your_domen/*** -  Меню
2. ***your_domen/show*** - Посмотреть все сообщения
3. ***your_domen/create-form*** - Создать сообщение
4. ***your_domen/edit?id=(цифра)*** - редактировать сообщение
