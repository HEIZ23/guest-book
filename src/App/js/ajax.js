let regEmail = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;
let regUsername = /^[a-z]+([-_]?[a-z0-9]+){0,2}$/i;
let regText = /^[?!,.а-яА-ЯёЁa-zA-Z0-9\s]+$/ui;
$(document).ready(function () {
    $("#file").change(function (event) {
        if (file.files.length == 0) {
            $("#text").prop('disabled', false);
        }
        if (file.files[0] != undefined) {
            if (file.files[0].type === 'text/plain') {
                if (file.files[0].size > 102400) {
                    alert("Вес текстового файла превышает 100кб. Выберите другой файл");
                    return;
                }
                $("#text").prop('disabled', true);

                let reader = new FileReader();

                reader.readAsText(file.files[0]);

                reader.onload = function () {
                    if (reader.result != '') {
                        text.value = reader.result;
                    } else {
                        alert('Текстовый файл пустой!');
                        return;
                    }
                };
            }
        }
    })
    $("#sendMessageForm").submit(function (event) {
        event.preventDefault();
        let fd = new FormData(this);
        let username = document.getElementById('Username');
        let email = document.getElementById('Email');
        let url = document.getElementById('Homepage');
        let text = document.getElementById('text');
        let browser = navigator.userAgent;
        let file = document.getElementById('file');
        if(file.files.length == 0 ){
            fd.delete('file');
        }
        if (file.files[0] != undefined) {

            if (file.files[0].type == 'text/plain') {
                if (file.files[0].size > 102400) {
                    alert("Вес текстового файла превышает 100кб");
                    return;
                }
                fd.delete('file');
            }
            if (file.files[0].type !== 'image/png'
                && file.files[0].type !== 'image/jpeg'
                && file.files[0].type !== 'image/gif'
                && file.files[0].type !== 'text/plain') {
                alert("Данный тип файла не поддерживается, попробуйте другой тип");
                return;

            } else if (file.files[0].size > 1000000) {
                alert("Вес изображения превышает 1мб");
                return;
            }
        }
        //validate input`s
        if (validateEmail(email.value) == false) {
            alert('Введите корректный email - email@mail.ru');
            return;
        }
        if (validateUsername(username.value) == false) {
            alert('Введите корректный username');
            return;
        }
        if (validateText(text.value) == false) {
            alert('Введите корректный текст в поле ввода');
            return;
        }


        fd.append('username', username.value);
        fd.append('email', email.value);
        fd.append('url', url.value);
        fd.append('text', text.value);
        fd.append('browser', browser);

        $.ajax({
            url: '/create',
            type: 'post',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                alert(JSON.parse(response).message);
                $("#sendMessageForm")[0].reset();
                location.href='/show';
            }
        })
    })
    $("#fileEdit").change(function (event) {

        if (fileEdit.files.length == 0) {
            $("#textEdit").prop('disabled', false);
        }
        if (fileEdit.files[0] != undefined) {
            if (fileEdit.files[0].size > 102400) {
                alert("Вес текстового файла превышает 100кб. Выберите другой файл.");
                return;
            }
            if (fileEdit.files[0].type === 'text/plain') {
                $("#textEdit").prop('disabled', true);

                let reader = new FileReader();

                reader.readAsText(fileEdit.files[0]);

                reader.onload = function () {
                    if (reader.result != '') {
                        textEdit.value = reader.result;
                    } else {
                        alert('Текстовый файл пустой!');
                        return;
                    }
                };
            }
        }
    })
    $("#editMessageForm").submit(function (event) {
        event.preventDefault();
        let fd = new FormData(this);
        let username = document.getElementById('usernameEdit').value;
        let email = document.getElementById('emailEdit').value;
        let url = document.getElementById('homepageEdit').value;
        let textEdit = document.getElementById('textEdit').value;
        let id = document.getElementById('id').value;
        let browser = navigator.userAgent;
        let fileEdit = document.getElementById('fileEdit');
        if(fileEdit.files.length == 0 ){
            fd.delete('file');
        }
        if (fileEdit.files[0] != undefined) {
            if (fileEdit.files[0].type == 'text/plain') {
                if (fileEdit.files[0].size > 102400) {
                    alert("Вес текстового файла превышает 100кб. Выбирите другой файл.");
                    return;
                }
                fd.delete('file');
            }
            if (fileEdit.files[0].type !== 'image/png'
                && fileEdit.files[0].type !== 'image/jpeg'
                && fileEdit.files[0].type !== 'image/gif'
                && fileEdit.files[0].type !== 'text/plain') {
                alert("Данный тип файла не поддерживается, попробуйте другой тип");
                return;

            } else if (fileEdit.files[0].size > 1000000) {
                alert("Вес изображения превышает 1мб");
                return;
            }
        }

        if(fileEdit.files.length = 0){
            fd.delete('file');
        }
        //validate input`s
        if (validateEmail(email) == false) {
            alert('Введите корректный email - email@mail.ru');
            return;
        }
        if (validateUsername(username) == false) {
            alert('Введите корректный username');
            return;
        }
        if (validateText(textEdit) == false) {
            alert('Введите корректный текст в поле ввода');
            return;
        }

        fd.append('username', username);
        fd.append('email', email);
        fd.append('url', url);
        fd.append('text', textEdit);
        fd.append('browser', browser);
        fd.append('id', id);

        $.ajax({
            url: '/update',
            type: 'post',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                alert(JSON.parse(response).message);
                location.href = '/show';
            },
            error: function (response) {
                alert(JSON.parse(response).message);
            }
        })
    })
})

function validateEmail(email) {
    return regEmail.test(email);
}

function validateUsername(username) {
    return regUsername.test(username);
}

function validateText(text) {
    return regText.test(text);
}

