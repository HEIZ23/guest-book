<?php

namespace App\Controller;

use App\Model\View;

/**
 * Контроллер для отображения страницы с ошибкой 404 и меню
 */
class HomeController
{
    /**
     *  Метод возвращает представление 404 ошибки
     * @return mixed
     */
    public function default(): mixed
    {
        http_response_code(404);
        json_encode(array("message" => "Некорректный запрос"), JSON_UNESCAPED_UNICODE);
        return View::render('404.php');
    }

    /**
     * Метод возвращает представление меню
     * @return mixed
     */
    public function menu(): mixed
    {
        return View::render('menu.php');
    }
}