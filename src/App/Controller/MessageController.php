<?php

namespace App\Controller;


use App\Model\DB;
use App\Model\Message;
use App\Model\View;

/**
 *Контроллер для работы с сообщениями
 * @property $pdo Активное соединение с базой данных
 * @property $data Массив с глобальными массивами GET, POST, SERVER
 */
class MessageController
{
    /**
     * Активное соединение с базой данных
     * @var DB
     */
    public $pdo;

    /**
     * Массив с глобальными массивами GET, POST, SERVER
     * @var
     */
    public $data;

    /**
     * Класс конструктор для контроллера MessageController
     * @param $pdo
     * @param $data
     */
    public function __construct($pdo, $data)
    {
        $this->pdo = $pdo;
        $this->data = $data;
    }

    /**
     * Метод-ендпоинт для создания сообщения
     * @return void
     */
    public function createMessage(): void
    {
        if ($this->data == null) {
            http_response_code(500);
            echo json_encode(array("message" => "Произошла ошибка при добавлении сообщение. Попробуйте позже"), JSON_UNESCAPED_UNICODE);
            return;
        }

        if ($this->data['server']["REQUEST_METHOD"] === "POST") {
            if (isset($this->data['files']['file'])) {
                if ($this->saveImage()['result'] == 'ok') {
                    $message = new Message($this->pdo);
                    $src = $this->saveImage()['src'];
                    $message->create(
                        $this->validateData($this->data['post']['username']),
                        $this->validateData($this->data['post']['email']),
                        $this->validateData($this->data['post']['url']),
                        $this->validateData($this->data['post']['text']),
                        $this->validateData($this->data['post']['browser']),
                        $this->validateData($this->data['server']["REMOTE_ADDR"]),
                        $src
                    );

                    http_response_code(200);
                    echo json_encode(array("message" => "Сообщение успешно добавлено"), JSON_UNESCAPED_UNICODE);
                } else {

                    http_response_code(500);
                    echo json_encode(array("message" => "Ошибка добавления сообщения - ошибка загрузки изображенияне"), JSON_UNESCAPED_UNICODE);
                }
            } else {

                $message = new Message($this->pdo);
                $message->create(
                    $this->validateData($this->data['post']['username']),
                    $this->validateData($this->data['post']['email']),
                    $this->validateData($this->data['post']['url']),
                    $this->validateData($this->data['post']['text']),
                    $this->validateData($this->data['post']['browser']),
                    $this->validateData($this->data['server']["REMOTE_ADDR"]),
                );

                http_response_code(200);
                echo json_encode(array("message" => "Сообщение успешно добавлено"), JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Метод для валидации входящих данных
     * @param $stringData "Входные данные из инпутов"
     * @return string
     */
    protected function validateData($stringData): string
    {
        return htmlspecialchars(trim($stringData));
    }

    /**
     * Метод для отображения сообщений
     * @return array
     */
    public function show(): array
    {
        $pagination = $this->data['get']['pagination'] ?? 1;
        $searchValue = $this->data['get']['search'] ?? '';
        $message = new Message($this->pdo);
        $countMessagesOnPage = 25;
        $offset = ($pagination - 1) * $countMessagesOnPage;
        $countMessagesInDB = $message->getCountRow()[0]['COUNT(*)'];
        $countPages = ceil($countMessagesInDB / $countMessagesOnPage);

        $this->doSearch($searchValue, $offset, $countMessagesOnPage, $countPages, $pagination);
        if (isset($this->data['get']['sort'])) {
            if ($this->getSortType() != null) {
                $_SESSION['data'] = [$message->getSortData($this->getSortType(), $offset, $countMessagesOnPage), $countPages, $pagination];
                View::render('showMessages.php');
                return $_SESSION['data'];
            }
        }

        $_SESSION['data'] = [$message->getAll($offset, $countMessagesOnPage), $countPages, $pagination, $searchValue];
        View::render('showMessages.php');
        return $_SESSION['data'];
    }

    /**
     * Метод для отображения формы редактирования сообщения
     * @return string|array
     */
    public function edit(): string|array
    {
        if (!isset($this->data['get']['id'])) {
            http_response_code(404);
            echo json_encode(array("message" => "Отсутствует id"), JSON_UNESCAPED_UNICODE);
            return [];
        }
        $message = new Message($this->pdo);
        $res = $message->getOne($this->data['get']['id']);
        if (!isset($res)) {
            return $res[0] = 'Такого сообщения нет!';
        }
        $_SESSION['message'] = $res;
        View::render('editMessage.php');
        return $_SESSION['message'];
    }

    /**
     * Метод-ендпоинт для выполнения обновления сообщения в базе даннных
     * @return void
     */
    public function update(): void
    {
        if ($this->data['server']["REQUEST_METHOD"] === "POST") {
            if (isset($this->data['files']['file'])) {
                if ($this->saveImage()['result'] == 'ok') {
                    $message = new Message($this->pdo);
                    $src = $this->saveImage()['src'];
                    $message->update(
                        $this->validateData($this->data['post']['username']),
                        $this->validateData($this->data['post']['email']),
                        $this->validateData($this->data['post']['url']),
                        $this->validateData($this->data['post']['text']),
                        $this->validateData($this->data['post']['browser']),
                        $this->validateData($this->data['server']["REMOTE_ADDR"]),
                        $this->validateData($this->data['post']["id"]),
                        $src
                    );
                    http_response_code(200);
                    echo json_encode(array("message" => "Сообщение успешно обновлено"), JSON_UNESCAPED_UNICODE);
                } else {
                    http_response_code(500);
                    echo json_encode(array("message" => "Сообщение не удалось обновить - ошибка загрузки изображения"), JSON_UNESCAPED_UNICODE);

                }


            } else {

                $message = new Message($this->pdo);
                $message->update(
                    $this->validateData($this->data['post']['username']),
                    $this->validateData($this->data['post']['email']),
                    $this->validateData($this->data['post']['url']),
                    $this->validateData($this->data['post']['text']),
                    $this->validateData($this->data['post']['browser']),
                    $this->validateData($this->data['server']["REMOTE_ADDR"]),
                    $this->validateData($this->data['post']["id"]),
                );
                http_response_code(200);
                echo json_encode(array("message" => "Сообщение успешно обновлено"), JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Метод для отображения формы создания сообщения
     * @return void
     */
    public
    function createForm(): void
    {
        View::render('createMessage.php');
    }

    /**
     * Метод для получения типа сортировки данных
     * @return string
     */
    public
    function getSortType(): string
    {
        if (isset($this->data['get']['sort'])) {
            $receivedType = $this->data['get']['sort'];
            return match ($receivedType) {
                'username_desc' => '`username` DESC',
                'username_asc' => '`username`',
                'email_desc' => '`email` DESC',
                'email_asc' => '`email`',
                'created_at_desc' => '`created_at` DESC',
                'created_at_asc' => '`created_at`',
                default => 'id'
            };
        } else {
            return $this->data['get']['sort'] = '`id`';
        }
    }

    /**
     * @param $searchValue "Значение поисковой строки"
     * @param $offset "Начальное значение выборки"
     * @param $countMessagesOnPage "Кол-во строк на странице"
     * @param $countPages "Кол-во страниц"
     * @param $pagination "Номер страницы"
     * @return array|void
     */
    public function doSearch($searchValue, $offset, $countMessagesOnPage, $countPages, $pagination)
    {
        if ($searchValue != '' && isset($this->data['get']['search'])) {
            $message = new Message($this->pdo);
            $_SESSION['data'] = [
                $message->getSearchResult($this->data['get']['search'], $offset, $countMessagesOnPage),
                $countPages,
                $pagination,
                $searchValue
            ];
            if (isset($this->data['get']['sort'])) {
                $_SESSION['data'] = [
                    $message->getSearchResult($this->data['get']['search'], $offset, $countMessagesOnPage, $this->getSortType()),
                    $countPages,
                    $pagination,
                    $searchValue
                ];
            }
            View::render('showMessages.php');
            return $_SESSION['data'];
        }
    }

    /**
     * Метод для сохранения изображения на сервере
     * @return array
     */
    public
    function saveImage()
    {

        $res = [];
        if (isset($this->data['files']['file'])) {
            $src = 'src/App/img/' . $this->data['files']['file']['name'];
            $res['src'] = $src;
            if (move_uploaded_file($this->data['files']['file']['tmp_name'], $src)) {
                $res['result'] = 'ok';
            } else {
                $res['result'] = 'no ok';
            }
        } else {
            $res['result'] = 'not';
        }

        return $res;
    }


}