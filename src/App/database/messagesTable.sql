CREATE DATABASE IF NOT EXISTS `guest-book`;

USE `guest-book`;

CREATE TABLE `messages`
(
    `id` INT(10) NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `url` VARCHAR(50) NOT NULL,
    `text` TEXT NOT NULL,
    `img_path` TEXT NULL,
    `browser` TEXT NULL,
    `ip` VARCHAR(50) NULL DEFAULT NULL,
    `created_at` INT(11) NOT NULL,
    `updated_at` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
)
    COMMENT ='таблица для сообщений'
    COLLATE = 'utf8mb4_general_ci'
    ENGINE = InnoDB
    AUTO_INCREMENT = 1
;

INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Petr', 'email@email.ru', 'https://facebook.com', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Anton', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1',UNIX_TIMESTAMP(), UNIX_TIMESTAMP() );
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Anton', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' ,UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1' , UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Anton', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Petr', 'email@email.ru', 'https://facebook.com', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Petr', 'email@email.ru', 'https://facebook.com', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Petr', 'email@email.ru', 'https://facebook.com', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());
INSERT INTO `messages` (username, email, url, text, browser, ip, created_at, updated_at)
VALUES ('Pavel', 'email@email.ru', 'https://vk.ru', 'Hello, world!', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36', '192.168.0.1', UNIX_TIMESTAMP(), UNIX_TIMESTAMP());

