<?php

namespace App\Model;

/**
 * /**
 * Модель для работы с сущностью сообщения
 * @property $id
 * @property $username
 * @property $url
 * @property $text
 * @property $email
 * @property $browser
 * @property $ip
 * @property $created_at
 * @property $updated_at
 * @property $pdo
 *
 */
class Message
{
    /**
     * Поле базы данных id(id сообщения)
     * @var
     */
    public $id;
    /**
     * Поле базы данных username(юзернейм отправителя)
     * @var
     */
    public $username;

    /**
     * Поле базы данных url(адрес домашней страницы или своего вебсайта)
     * @var
     */
    public $url;
    /**
     * Поле базы данных text(текст сообщения)
     * @var
     */
    public $text;
    /**
     * Поле базы данных email(адрес электронной почты)
     * @var
     */
    public $email;
    /**
     * Поле базы данных browser(информация о браузере)
     * @var
     */
    public $browser;
    /**
     * Поле базы данных ip(ip адрес)
     * @var
     */
    public $ip;
    /**
     * Поле базы данных created_at(дата и время cоздание)
     * @var
     */
    public $created_at;
    /**
     * Поле базы данных updated_at(дата и время обновления)
     * @var
     */
    public $updated_at;
    /**
     * @var DB
     */
    public $pdo;

    /**
     * Метод создания объекта класса
     * @param DB $pdo
     */
    public function __construct(DB $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Метод создания сообщения
     * @param $username "Юзернейм отправителя"
     * @param $email "Адрес электронной почты"
     * @param $url "Адрес домашней страницы или вебсайта"
     * @param $text "Текст сообщения"
     * @param $browser "Информация о браузере"
     * @param $ip "Ip адрес"
     * @return void
     */
    public function create($username, $email, $url, $text, $browser, $ip, $imgPath = null): void
    {
        $this->pdo->query("INSERT INTO messages(username, email, url, text, browser, ip, created_at, updated_at, img_path) 
                                VALUES (?,?,?,?,?,?,?,?,?)",
            [
                $username,
                $email,
                $url,
                $text,
                $browser,
                $ip,
                time(),
                time(),
                $imgPath
            ]);
    }

    /**
     * Метод возвращает все сообщения из базы данных
     * @param $offset "Начальное значение выборки"
     * @param $countMessages "Кол-во сообщений на странице"
     * @return array|false
     */
    public function getAll($offset, $countMessages): array|false
    {
        $res = $this->pdo->query("SELECT * FROM messages  LIMIT {$offset},{$countMessages}");
        return $res->fetchAll();
    }

    /**
     * Метод возвращает одно сообщение по id
     * @param $id "id сообщения"
     * @return array|false
     */
    public function getOne($id): array|false
    {
        $res = $this->pdo->query("SELECT * FROM messages WHERE id=(?)", [$id]);
        return $res->fetchAll();
    }

    /**
     * Метод редактирования сообщения
     * @param $username "Юзернейм отправителя"
     * @param $email "Адрес электронной почты"
     * @param $url "Адрес домашней страницы или вебсайта"
     * @param $text "Текст сообщения"
     * @param $browser "Информация о браузере"
     * @param $ip "Ip адрес"
     * @param $id "id сообщения"
     * @return void
     */
    public function update($username, $email, $url, $text, $browser, $ip, $id, $imgPath = null): void
    {
        $res = $this->pdo->query("UPDATE messages 
                                      SET username = ?,
                                          email = ?,
                                          url = ?,
                                          text = ?,
                                          browser = ?,
                                          ip = ?,
                                          updated_at = ?,
                                          img_path = ?
                                      WHERE id = ?",
            [
                $username,
                $email,
                $url,
                $text,
                $browser,
                $ip,
                time(),
                $imgPath,
                $id
            ]);
    }

    /**
     * Метод получение отсортированных данных
     * @param $sortType "Метод сортировки"
     * @param $offset "Начальное значение выборки"
     * @param $countMessages "Кол-во сообщений на странице"
     * @return array|false
     */
    public function getSortData($sortType, $offset, $countMessages): array|false
    {
        //Если будет время продебагать почему нормально не работает с подставлением аргументов
        $res = $this->pdo->query("SELECT * FROM messages ORDER BY {$sortType} LIMIT {$offset},{$countMessages}");
        return $res->fetchAll();
    }

    /**
     * Метод для получения кол-во строк в таблице
     * @return array|false
     */
    public function getCountRow(): array|false
    {
        $res = $this->pdo->query("SELECT COUNT(*) FROM messages");

        return $res->fetchAll();
    }

    /**
     * Метод возвращает результаты поиска
     * @param $inputValue "Значение из поисковой строки"
     * @param $offset "Начальное значение выборки"
     * @param $countMessages "Кол-во сообщений на странице"
     * @param $sortType "Метод сортировки"
     * @return array|false
     */
    public function getSearchResult($inputValue, $offset, $countMessages, $sortType = null): array|false
    {

        $res = $this->pdo->query("SELECT *
                                      FROM messages 
                                      WHERE `username` LIKE '%$inputValue%'
                                      OR `email` LIKE '%$inputValue%'
                                      OR `text` LIKE  '%$inputValue%'
                                      LIMIT {$offset},{$countMessages}
                                      ");
        if ($sortType != null) {
            $res = $this->pdo->query("SELECT *
                                      FROM messages 
                                      WHERE `username` LIKE '%$inputValue%'
                                      OR `email` LIKE '%$inputValue%'
                                      OR `text` LIKE '%$inputValue%'
                                      ORDER BY {$sortType}
                                      LIMIT {$offset},{$countMessages}
                                      ");
        }
        return $res->fetchAll();
    }
}