<?php

namespace App\Model;

use PDO;

/**
 * Модель для работы с базой данных
 * @property $conn
 */
class DB
{
    /**
     * @var PDO
     */
    public $conn;

    /**
     * Метод создания объекта класса для работы с базой данных
     * @param $db "Имя базы данных"
     * @param $username "Юзернейм"
     * @param $password "Пароль"
     * @param $host "Хост"
     * @param $port "Порт"
     * @param $options "Опции"
     */
    public function __construct($db, $username = NULL, $password = NULL, $host = '127.0.0.1', $port = 3306, $options = [])
    {
        //Наверняка лучшей практикой было бы что бы только конструктор знал о данных из конфига...
        //Может быть инклюдить именно в контруктор, но пока так
        //Хотя при создании объекта требуется передавать переменные(хост и т.д)
        //!!! Были попытки сделать через иклюд файла конфига, но возращает неверные данные

        if (!$options) {
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => FALSE
            ];
        }

        $dsn = "mysql:host=" . $host . "; dbname=" . $db . "; port=" . $port . "; charset=utf8mb4";
        $this->conn = new PDO($dsn, $username, $password, $options);
    }

    /**
     * Метод для выполнения запросов к базе данных
     * @param $sql "Текст запроса"
     * @param $arguments "Переменные"
     * @return false|\PDOStatement
     */
    public function query($sql, $arguments = NULL): false|\PDOStatement
    {
        if (!$arguments) {
            return $this->conn->query($sql);
        }

        $stmt = $this->conn->prepare($sql);
        $stmt->execute($arguments);

        return $stmt;
    }
}