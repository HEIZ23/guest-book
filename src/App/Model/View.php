<?php

namespace App\Model;

/**
 * Класс для генерации представлений
 */
class View
{
    /**
     * Метод для генерации представлений
     * @param $path "Название и путь к файлу представления"
     * @return mixed
     */
    public static function render($path): mixed
    {
        return require_once 'src/App/View/' . $path;
    }
}