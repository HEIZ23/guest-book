<?php

return [
    'DB_NAME' => 'guest-book',
    'DB_HOST' => 'localhost',
    'DB_USERNAME' => 'root',
    'DB_PASSWORD' => '',
    'CHARSET' => 'utf8mb4_general_ci',
    'PORT' => '3306'
];
