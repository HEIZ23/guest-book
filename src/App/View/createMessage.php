<?php include 'baseTemplate.php' ?>


<body>
<div class="d-flex justify-content-center align-items-center vh-100">
    <div class="border border-dark rounded-3 shadow">
        <form class="px-3 py-3" id="sendMessageForm">
            <div class="mb-3">
                <label for="Username" class="form-label">Username</label>
                <input type="text" name="username" class="form-control shadow-sm" id="Username"
                       placeholder="Введите username" required minlength="3">
            </div>
            <div class="mb-3">
                <label for="Email" class="form-label">Email</label>
                <input type="text" name="email" class="form-control shadow-sm" id="Email"
                       placeholder="Введите адрес электронной почты" required>
            </div>
            <div class="mb-3">
                <label for="Homepage" class="form-label">Homepage</label>
                <input type="text" name="url" class="form-control shadow-sm" id="Homepage"
                       placeholder="Введите формат url">
            </div>
            <div class="mb-3">
                <label for="image" class="form-label">add File</label>
                <input type="file" name="file" class="form-control shadow-sm" id="file"
                       placeholder="Выберите файл">
            </div>
            <div class="mb-3">
                <label for="text" class="form-label">Text</label>
                <textarea class="form-control" id="text" rows="3" required minlength="10"></textarea>
            </div>
            <button type="submit" class="w-100 btn btn-primary d-block mx-auto shadow-sm">Send message</button>
            <a href="/" class="btn btn-outline-danger w-100 mt-2">Вернуться в меню</a>
        </form>
    </div>
</div>
</body>
