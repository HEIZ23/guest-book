<?php include 'baseTemplate.php';?>
<div class="d-inline text-center">
    <h1>404 Not found</h1>
    <h3>Такой страницы не существует</h3>
    <a href="/" class=" d-flex justify-content-center  mx-auto border-none btn btn-outline-secondary w-25">Вернутся на главную...</a>
</div>
