<?php
include 'baseTemplate.php';
$data = $_SESSION['message'][0];
/*$data['username']
 $data['email']
 $data['url']
 $data['text'] */
?>

<body>
<div class="d-flex justify-content-center align-items-center vh-100">
    <div class="border border-dark rounded-3 shadow">
        <form class="px-3 py-3" id="editMessageForm">
            <div class="mb-3">
                <label for="usernameEdit" class="form-label">Username</label>
                <input type="text" name="username" class="form-control shadow-sm" id="usernameEdit"
                       placeholder="Введите username"  minlength="3" value="<?php echo $data['username'];?>">
            </div>
            <div class="mb-3">
                <label for="emailEdit" class="form-label">Email</label>
                <input type="text" name="email" class="form-control shadow-sm" id="emailEdit"
                       placeholder="Введите адрес электронной почты" required value="<?php echo $data['email'];?>">
            </div>
            <div class="mb-3">
                <label for="homepageEdit" class="form-label">Homepage</label>
                <input type="text" name="url" class="form-control shadow-sm" id="homepageEdit"
                       placeholder="Введите формат url" value="<?php echo $data['url'];?>">
            </div>
            <div class="mb-3">
                <label for="file" class="form-label">add File</label>
                <input type="file" name="file" class="form-control shadow-sm" id="fileEdit"
                       placeholder="Выберите файл" value="<?php echo $data['img_path']; ?>">
            </div>
            <div class="mb-3">
                <label for="textEdit" class="form-label">Text</label>
                <input type="text" class="form-control" id="textEdit" required value="<?php echo $data['text'];?>">
            </div>
            <div class="mb-3">
                <input type="hidden" class="form-control" id="id" required value="<?php echo $data['id'];?>">
            </div>
            <button type="submit" class="w-100 btn btn-primary d-block mx-auto shadow-sm">Send message</button>
            <a href="/show" class="btn btn-outline-danger w-100 mt-2">Вернуться назад</a>
        </form>
    </div>
</div>
</body>
