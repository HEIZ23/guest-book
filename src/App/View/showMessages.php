<?php
include 'baseTemplate.php';
date_default_timezone_set('Europe/Moscow');
$data = $_SESSION['data'][0];

$countPages = $_SESSION['data'][1];
$pagination = $_SESSION['data'][2];
if(!isset($_GET['sort'])){
    $_GET['sort'] = 'id';
}

if(!isset($_GET['search'])){
    $_GET['search'] = $_SESSION['data'][3];
}
function generateSortLinks($title, $a, $b, $pagination)
{
    if (isset($_GET['sort'])) {
        if ($_GET['sort'] == $a) {
            return "<a href='/show?sort={$b}&pagination={$pagination}&search={$_GET['search']}' class='text-decoration-none text-black'>{$title} <i>▼</i></a>";
        } elseif ($_GET['sort'] == $b) {
            return "<a href='/show?sort={$a}&pagination={$pagination}&search={$_GET['search']}' class='text-decoration-none text-black'>{$title} <i>▲</i></a>";
        } else {
            return "<a href='/show?sort={$a}&pagination={$pagination}&search={$_GET['search']}' class='text-decoration-none text-black'>{$title} </a>";
        }
    }
    return "<a href='/show?sort={$a}&pagination=1&search={$_GET['search']}' class='text-decoration-none text-black'>{$title}</a>";
}


?>
<div class="d-flex justify-content-around my-2">
    <a href="/" class="btn btn-outline-secondary">Вернуться в меню</a>
    <form name="search" id="searchForm" action="/show">
        <div class="d-flex">
            <input type="text" id="searchInput" name="search" placeholder="Поиск" class="form-control" minlength="3">
            <button type="submit" class="btn btn-secondary ms-2">Найти</button>
        </div>

    </form>
</div>


<table class="table text-center">
    <thead>
    <tr>

        <th scope="col">№</th>
        <th scope="col"><?php echo generateSortLinks('username', 'username_asc', 'username_desc', $pagination); ?></th>
        <th scope="col"><?php echo generateSortLinks('email', 'email_asc', 'email_desc', $pagination); ?></th>
        <th scope="col">Text</th>
        <th scope="col"><?php echo generateSortLinks('created_at', 'created_at_asc', 'created_at_desc', $pagination); ?></th>
        <th scope="col">Buttons</th>

    </tr>

    </thead>
    <tbody>
    <?php
    if (empty($data)) {
        echo "<td colspan='6' class='fw-bold'>По вашему запросу ничего не найдено</td>";
        return;
    }

    foreach ($data as $item) {
        $time = date('d.m.y D, d M Y H:i:s', $item['created_at']);

        $img = isset($item['img_path']) && $item['img_path'] != '' && is_readable($item['img_path']) ? "<img src='{$item['img_path']}' style='max-width:320px; max-height=240px'>" : '';

        echo '<tr>';

        echo "<td class='align-middle'>{$item['id']}</td>";
        echo "<td class='align-middle'>{$item['username']}</td>";
        echo "<td class='align-middle'>{$item['email']}</td>";
        echo "<td class='align-middle'>{$img}{$item['text']}</td>";
        echo "<td class='align-middle'>$time</td>";
        echo "<td><a class='btn btn-outline-secondary' href='/edit?id={$item['id']}' role='button'><img src='src/App/img/edit.svg' alt='Кнопка редактировать'></a></td>";
        echo '</tr>';
    }
    ?>

    </tbody>
</table>
<ul class="pagination d-flex justify-content-center">
    <li><a href="<?php if (isset($_GET['sort'])) {
            echo "?sort={$_GET['sort']}&pagination=1&search={$_GET['search']}";
        } else {
            echo "?pagination=1&search={$_GET['search']}";
        }
        ?>" class="btn btn-outline-secondary mx-2">First</a></li>
    <li class="<?php if ($pagination <= 1) {
        echo 'disabled';
    } ?>">
        <a href="<?php if ($pagination <= 1) {
            echo '#';
        } else {
            if (isset($_GET['sort'])) {
                echo "?sort={$_GET['sort']}&pagination=" . ($pagination - 1) . "&search={$_GET['search']}";
            } else {
                echo "?pagination=" . ($pagination - 1) . "&search={$_GET['search']}";
            }

        } ?>" class="btn btn-outline-secondary mx-2">Prev</a>
    </li>
    <li class="<?php if ($pagination >= $countPages) {
        echo 'disabled';
    } ?>">
        <a href="<?php if ($pagination >= $countPages) {
            echo '#';
        } else {
            if (isset($_GET['sort'])) {
                echo "?sort={$_GET['sort']}&pagination=" . ($pagination + 1). "&search={$_GET['search']}";
            } else {
                echo "?pagination=" . ($pagination + 1). "&search={$_GET['search']}";
            }

        } ?>" class="btn btn-outline-secondary mx-2">Next</a>
    </li>
    <li><a href="<?php if (isset($_GET['sort']) || isset($_GET['search'])) {
            echo "?sort={$_GET['sort']}&pagination={$countPages}&search={$_GET['search']}";
        } else {
            echo "?pagination={$countPages}&search={$_GET['search']}";
        }
        ?>" class="btn btn-outline-secondary mx-2">Last</a></li>
</ul>

